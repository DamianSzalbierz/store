class AddShelfToProducts < ActiveRecord::Migration[5.0]
  def change
    add_column :products, :shelf, :boolean, default: true
  end
end

Rails.application.routes.draw do
  scope '(:locale)', locale: /#{I18n.available_locales.join("|")}/ do
    root 'shelf#index', via: :all
    devise_for :users, :controllers => {:registrations => "registrations"}
    resources :orders
    resources :line_items, only: :create
    resources :carts, only: [:show, :destroy]
  end

  resources :products do
    get :who_bought, on: :member
    patch :toggle_shelf, on: :member
  end

  resources :users_admin, except: :update, controller: 'users' do
    get 'profile', on: :member
  end
end

I18n.config.available_locales = [:en, :pl]

I18n.default_locale = :pl

LANGUAGES = [
    ['English', 'en'],
    ['Polski', 'pl']
]
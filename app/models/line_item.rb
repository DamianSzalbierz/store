class LineItem < ApplicationRecord
  belongs_to :product, optional: true
  belongs_to :cart
  belongs_to :order, optional: true

  validates_presence_of :product_id

  def price_calculation
    product.price * quantity
  end
end

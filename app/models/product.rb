class Product < ApplicationRecord
  before_destroy :line_item_do_not_exist?

  has_attached_file :image, styles: { medium: "300x300>", thumb: "100x100>" },
                    default_url: "/images/:style/missing.png"

  validates_attachment_content_type :image, content_type: /\Aimage\/.*\z/
  validates_presence_of :title, :description, :price

  belongs_to :user
  has_many :line_items
  has_many :orders, through: :line_items

  translates :title, :description

  def self.search(search)
    if search
      where("title LIKE ? AND shelf = ?", "%#{search}%", true).order(:title)
    else
      where(shelf: true).order(:title)
    end
  end

  private

  def line_item_do_not_exist?
    unless line_items.empty?
      errors.add('base', 'Przedmiot jest w koszyku!')
      throw :abort
    end
  end
end

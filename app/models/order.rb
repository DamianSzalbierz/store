class Order < ApplicationRecord
  has_many :line_items, dependent: :destroy

  enum pay_type: [:check, :credit_card, :purchase_order]

  validates :name, :address, :email, presence: true
  validates :pay_type, inclusion: pay_types.keys

  def self.pay_types_i18n
    pay_types.keys.map do |pay_type|
      [I18n.t("activerecord.attributes.order.pay_types.#{pay_type}"), pay_type]
    end
  end

  def add_line_items_from_cart(cart)
    cart.line_items.each do |item|
      item.cart_id = nil
      line_items.push(item)
    end
  end
end

class ShelfController < ApplicationController
  skip_before_action :authenticate_user!

  def index
    @products = Product.search(params[:search])
  end
end

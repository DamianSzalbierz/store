class ProductsController < ApplicationController
  before_action :set_product, only: [:show, :edit, :update, :destroy, :toggle_shelf]

  def index
    @products = Product.all
  end

  def new
    @product = Product.new
  end

  def create
    @product = Product.new(product_params)
    @product.user = current_user
    if @product.save
      flash[:success] = "Product has been created by #{current_user.email}"
      redirect_to products_path
    else
      flash.now[:danger] = "Product has not been created"
      render :new
    end
  end

  def show
  end

  def edit
    unless @product.user == current_user || current_user.admin?
      flash[:alert] = "You can only edit your own product."
      redirect_to root_path
    end
  end

  def update
    unless @product.user == current_user
      flash[:danger] = "You can only edit your own product."
      redirect_to root_path
    else
      if @product.update(product_params)
        flash[:notice] = "Product has been updated"
        redirect_to @product
      else
        flash.now[:danger] = "Product has not been updated"
        render :edit
      end
    end
  end

  def destroy
    unless @product.user == current_user
      flash[:alert] = "You can only delete your own products."
      redirect_to root_path
    else
      if @product.destroy
        flash[:success] = "Product has been deleted"
        redirect_to products_path
      end
    end
  end

  # def who_bought
  #   @product = Product.find(params[:id])
  #   @latest_order = @product.orders.order(:updated_at).last
  #
  #   if stale?(@latest_order)
  #     respond_to do |format|
  #       format.atom
  #       format.json { render json: @product.to_json(include: :orders) }
  #     end
  #   end
  # end

  def toggle_shelf
    @product.toggle(:shelf)
    if @product.save
      redirect_to products_path
    end
  end

  private

  def set_product
    @product = Product.find(params[:id])
  end

  def product_params
    params.require(:product).permit(:title, :description, :price, :image)
  end

  protected

  def resource_not_found
    message = "The product you are looking for could not be found"
    flash[:alert] = message
    redirect_to root_path
  end
end

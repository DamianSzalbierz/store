class LineItemsController < ApplicationController
  include CurrentCart
  before_action :set_cart, only: :create
  skip_before_action :authenticate_user!, only: :create

  # POST /line_items
  # POST /line_items.json
  def create
    product = Product.find(params[:product_id])
    @line_item = @cart.add_product(product)

    respond_to do |format|
      if @line_item.save
        format.html {redirect_to @line_item.cart, notice: 'Line item was successfully created.'}
        format.json {render :show, status: :created, location: @line_item}
      else
        format.html {redirect_to @line_item.cart, flash: {danger: 'Line item wasn\'t created.'}}
      end
    end
  end
end

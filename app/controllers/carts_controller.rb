class CartsController < ApplicationController
  before_action :set_cart, only: [:show, :destroy]
  skip_before_action :authenticate_user!, only: [:show, :destroy]

  # GET /carts/1
  # GET /carts/1.json
  def show
  end

  # DELETE /carts/1
  # DELETE /carts/1.json
  def destroy
    @cart.destroy if @cart.id = session[:cart_id]
    session[:cart_id] = nil
    respond_to do |format|
      format.html { redirect_to root_path, notice: 'Koszyk jest pusty.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_cart
      @cart = Cart.find(params[:id])
    end

  protected

  def resource_not_found
    logger.error "Attempt to access invalid cart #{params[:id]}"
    message = "The cart you are looking for could not be found"
    flash[:alert] = message
    redirect_to root_path
  end
end

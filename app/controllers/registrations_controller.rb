class RegistrationsController < Devise::RegistrationsController
  def new
    super
  end

  def create
    super
    # add custom create logic here
    @users = User.all
    if User.any?
      User.order(:created_at).last.update_attribute(:admin, true) if !@users.admin_exist?
    end
  end

  def update
    super
  end

  # private
  #
  # # Do not needed
  # def sign_up_params
  #   params.require(:user).permit(:name, :email, :password, :password_confirmation)
  # end
  #
  def account_update_params
    params.require(:user).permit(:name, :email, :password, :password_confirmation, :current_password)
  end

end
require "rails_helper"

RSpec.describe OrderMailer, type: :mailer do
  describe "received" do
    let(:user) { User.create(email: 'user@example.com', password: 'foobar') }
    let(:order) { Order.create(name: 'User Example', address: 'Warszawa', email: 'user@example.com', pay_type: 2) }
    let(:mail) { OrderMailer.received(order).deliver_now }

    it "renders the headers" do
      expect(mail.subject).to eq("DSStore Order Confirmation")
      expect(mail.to).to eq([order.email])
      expect(mail.from).to eq(["szalbierz.d.k@gmail.com"])
    end

    it "renders the body" do
      expect(mail.body.encoded).to match("Dziekuje za zakup")
    end
  end
end

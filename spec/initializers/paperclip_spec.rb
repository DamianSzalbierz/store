require 'rails_helper'

describe 'Paperclip config file' do
  it 'test env' do
    unless Rails.env == 'test'
      expect(Paperclip::Attachment.default_options[:url]).to eq(':s3_domain_url')
      expect(Paperclip::Attachment.default_options[:path]).to eq('/:class/:attachment/:id_partition/:style/:filename')
      expect(Paperclip::Attachment.default_options[:s3_host_name]).to eq('s3.eu-central-1.amazonaws.com')
    end
  end
end
require "rails_helper"

RSpec.describe CartsController, type: :routing do
  describe "routing" do
    it "routes to #show" do
      expect(:get => "/carts/1").to route_to("carts#show", :id => "1")
    end

    it "routes to #destroy" do
      expect(:delete => "/carts/1").to route_to("carts#destroy", :id => "1")
    end

  end
end

require 'rails_helper'

RSpec.describe ProductsController, type: :controller do

  describe 'tootle product' do
    let(:admin) {User.create(email: 'admin@example.com', password: 'foobar')}
    let!(:product) { Product.create(title: 'Produkt 1', description: 'Opis produktu', price: 21.00, user: admin) }

    it 'toggle product shelf attribute' do
      sign_in admin
      expect(product.shelf).to eq true
      patch :toggle_shelf, params: { id: product.id }
      expect(assigns(:product).shelf).to eq false
      expect(response).to redirect_to products_path
    end
  end

end

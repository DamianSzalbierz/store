require 'rails_helper'

RSpec.describe OrdersController, type: :controller do

  describe 'update order' do
    let(:admin) {User.create(email: 'admin@example.com', password: 'foobar')}
    let!(:order) {Order.create(name: 'User', email: admin.email, address: 'Address', pay_type: 'check')}

    context 'with valid order' do
      it 'update order and redirect' do
        sign_in admin
        patch :update, params: { id: order.id,  order: { name: 'Tomek', address: 'Poznan', email: 'tomek@example.com', pay_type:'check' } }
        expect(assigns(:order).name).to eq('Tomek')
        expect(assigns(:order).address).to eq('Poznan')
        expect(assigns(:order).email).to eq('tomek@example.com')
        expect(flash[:notice]).not_to be_nil
        expect(response).to redirect_to(order_path('pl', order.id))
      end
    end

    context 'with invalid order' do
      it 'fail to update order and render edit' do
        sign_in admin
        patch :update, params: { id: order.id, order: { name: '', address: 'Poznan', email: 'tomek@example.com', pay_type:'check' } }
        expect(assigns(:order)).to eq(order)
        expect(flash[:notice]).to be_nil
        expect(response).to render_template(:edit)
      end
    end
  end

end
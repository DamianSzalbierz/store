require 'rails_helper'

RSpec.describe LineItemsController, type: :controller do

  describe 'create line item' do
    it 'redirect after failing when product do not exist' do
      post :create, params: { product_id: nil, line_item: {  } }
      expect{ LineItem.count }.to change { LineItem.count }.by(0)
    end
  end
end

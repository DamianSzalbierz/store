require 'rails_helper'

RSpec.feature 'Add products to cart' do
  let(:admin) {User.create(email: "damian@example.com", password: "password", admin: true)}
  let!(:product) {Product.create(title: "Product 1", description: "Lorem ipsum dolor sit amet, consectetur.", price: 19.99, user: admin)}

  describe 'Adding products to cart' do
    it 'successfully added product' do
      visit '/'
      click_link "Dodaj do koszyka"
      expect(page.current_path).to eq(cart_path('pl', Cart.last.id))
      expect(Cart.last.line_items.length).to eq(1)
    end

    it 'continue to add product after first product added' do
      visit '/'
      click_link "Dodaj do koszyka"
      expect(page.current_path).to eq(cart_path('pl', Cart.last.id))
      expect(Cart.last.line_items.length).to eq(1)
      click_link 'Back to Shelf'
      expect(page.current_path).to eq(root_path('pl'))
      click_link "Dodaj do koszyka"
      expect(page.current_path).to eq(cart_path('pl', Cart.last.id))
      expect(Cart.last.line_items.length).to eq(1)
      expect(Cart.last.line_items.first.quantity).to eq(2)
    end
  end
end
require 'rails_helper'

RSpec.feature 'Remove products from cart' do
  let(:admin) {User.create(email: "damian@example.com", password: "password", admin: true)}
  let!(:product) {Product.create(title: "Product 1", description: "Lorem ipsum dolor sit amet, consectetur.", price: 19.99, user: admin)}

  describe 'Delete all products' do
    it do
      visit '/'
      click_link "Dodaj do koszyka"
      expect(page.current_path).to eq(cart_path('pl', Cart.last.id))
      click_button 'Empty Cart'
      expect(page.current_path).to eq(root_path('pl'))
      expect(Cart.any?).to eq false
    end
  end
end
require 'rails_helper'

RSpec.feature 'Listing users' do
  let(:admin) { User.create(email: "damian@example.com", password: "password", admin: true) }
  before { login_as admin }

  scenario 'Listing all users' do
    visit '/'
    click_link 'Użytkownicy'
    expect(page.current_path).to eq(users_admin_index_path)
    within('h1') do
      expect(page).to have_content('Użytkownicy')
    end
    within('table') do
      expect(page).to have_content('Imię i nazwisko')
      expect(page).to have_content('Email')
      expect(page).to have_content('Akcje')
      expect(page).to have_content('Pokaż')
      expect(page).to have_content('Usuń')
      expect(page).to have_content('damian@example.com')
    end
    expect(page).to have_link('Nowy użytkownik')
  end
end
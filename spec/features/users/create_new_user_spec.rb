require 'rails_helper'

RSpec.feature 'Create user' do
  let(:admin) {User.create(email: "damian@example.com", password: "password", admin: true)}
  before do
    login_as admin

    visit new_users_admin_path
    within('h1') do
      expect(page).to have_content('New User')
    end
    within('form#new_user') do
      expect(page).to have_field('user[email]')
      expect(page).to have_field('user[name]')
      expect(page).to have_field('user[password]')
      expect(page).to have_field('user[password_confirmation]')
    end
    expect(page).to have_button('Utwórz')
    expect(page).to have_link('Back')
  end

  scenario 'admin successfully create user' do
    within('form#new_user') do
      fill_in 'user[email]', with: 'user@example.com'
      fill_in 'user[name]', with: 'Example'
      fill_in 'user[password]', with: 'foobar'
      fill_in 'user[password_confirmation]', with: 'foobar'
    end
    click_button 'Utwórz'
    expect(page.current_path).to eq(users_admin_index_path)
    expect(page).to have_content('User was successfully created.')
  end

  scenario 'admin fail to create user' do
    click_button 'Utwórz'
    expect(page.current_path).to eq(users_admin_index_path)
    expect(page).to have_content('prohibited this user from being saved:')
    within('form#new_user') do
      expect(page).to have_field('user[email]')
      expect(page).to have_field('user[name]')
      expect(page).to have_field('user[password]')
      expect(page).to have_field('user[password_confirmation]')
    end
    expect(page).to have_button('Utwórz')
    expect(page).to have_link('Back')
  end

  scenario 'admin resign to create user' do
    click_link 'Back'
    expect(page.current_path).to eq(users_admin_index_path)
  end
end
require 'rails_helper'

RSpec.feature 'Delete user' do
  let!(:admin) {User.create(email: "damian@example.com", password: "password", admin: true)}
  let!(:user) {User.create(email: "user@example.com", password: "password")}
  before {login_as admin}

  scenario 'Delete user from users index page' do
    visit '/'
    click_link 'Użytkownicy'
    expect(page.current_path).to eq(users_admin_index_path)
    within('table') do
      expect(page).to have_content('damian@example.com')
      expect(page).to have_content('user@example.com')
    end
    within('table tr:nth-child(2)') do
      click_link 'Usuń'
    end
    expect(page.current_path).to eq(users_admin_index_path)
    within('table') do
      expect(page).not_to have_content('user@example.com')
    end
  end

  scenario 'Delete user from users show page' do
    visit '/'
    click_link 'Użytkownicy'
    expect(page.current_path).to eq(users_admin_index_path)
    within('table') do
      expect(page).to have_content('damian@example.com')
      expect(page).to have_content('user@example.com')
    end
    within('table tr:nth-child(2)') do
      click_link 'Pokaż'
    end
    expect(page.current_path).to eq(users_admin_path(user.id))
    click_link 'Usuń'
    expect(page.current_path).to eq(users_admin_index_path)
    within('table') do
      expect(page).not_to have_content('user@example.com')
    end
  end
end
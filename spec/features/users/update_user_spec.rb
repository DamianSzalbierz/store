require 'rails_helper'

RSpec.feature 'Update user' do
  let(:user) {User.create(email: "damian@example.com", password: "password", admin: false)}
  before do
    login_as user

    visit edit_user_registration_path
    within('h2') do
      expect(page).to have_content('Edit User')
    end
    within('form#edit_user') do
      expect(page).to have_field('user[email]')
      expect(page).to have_field('user[name]')
      expect(page).to have_field('user[password]')
      expect(page).to have_field('user[password_confirmation]')
    end
    expect(page).to have_button('Aktualizuj')
    expect(page).to have_button('Cancel my account')
    expect(page).to have_link('Back')
  end

  scenario 'user successfully updete profile' do
    within('form#edit_user') do
      fill_in 'user[name]', with: 'Example'
      fill_in 'user[current_password]', with: 'password'
    end
    click_button 'Aktualizuj'
    expect(page.current_path).to eq(root_path('pl'))
    expect(page).to have_content('Twoje konto zostało zaktualizowane pomyślnie.')
  end
end
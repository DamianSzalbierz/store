require 'rails_helper'

RSpec.feature 'User profile' do
  let!(:admin) {User.create(email: "damian@example.com", password: "password", admin: true)}
  let!(:user) {User.create(email: "duser@example.com", password: "password")}

  scenario 'Show admin profile' do
    login_as admin
    visit '/'
    click_link 'Panel'
    expect(page.current_path).to eq(profile_users_admin_path(admin.id))
    expect(page).to have_link('Edytuj')
  end

  scenario 'Show admin profile' do
    login_as user
    visit '/'
    click_link 'Panel'
    expect(page.current_path).to eq(profile_users_admin_path(user.id))
    expect(page).to have_link('Edytuj')
  end
end
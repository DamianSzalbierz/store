require 'rails_helper'

RSpec.feature 'Delete order feature' do
  describe 'deleting order' do
    let(:admin) {User.create(email: 'admin@example.com', password: 'foobar', admin: true)}
    let!(:order) {Order.create(name: 'User', email: admin.email, address: 'Address', pay_type: 1)}
    before { login_as admin }

    it 'delete order' do
      visit orders_path
      click_link 'Usuń'
      expect(page).to have_content('Order was successfully destroyed.')
    end
  end
end
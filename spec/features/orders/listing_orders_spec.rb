require 'rails_helper'

RSpec.feature 'Listing Orders' do
  describe 'Admin listing orders' do
    let(:admin) {User.create(email: 'admin@example.com', password: 'foobar')}
    let!(:order) {Order.create(name: 'User', email: admin.email, address: 'Address', pay_type: 1)}
    before {login_as admin}

    it 'show all orders' do
      visit orders_path
      within('table') do
        expect(page).to have_content("Nazwa")
        expect(page).to have_content("User")
        expect(page).to have_link("Usuń")
      end
    end
  end
end
require 'rails_helper'

RSpec.feature 'Create Order' do
  describe 'creating order' do
    let(:admin) {User.create(email: 'admin@example.com', password: 'foobar')}
    let!(:product) {Product.create(title: 'Produkt', description: 'Opis produktu', price: 12.99, user: admin)}

    context 'successful' do
      it 'create new order' do
        visit '/'

        click_link 'Dodaj do koszyka'
        click_link 'Checkout'

        expect(page.current_path).to eq(new_order_path('pl'))

        within('form#new_order') do
          fill_in 'Nazwa', with: 'User Example'
          fill_in 'Adres', with: 'Example adres'
          fill_in 'Email', with: 'user@example.com'
          select 'Czek', from: 'order[pay_type]'
          click_button 'Utwórz'
        end

        expect(page.current_path).to eq(root_path('pl'))
        expect(Order.last.email).to eq('user@example.com')
        expect(Order.last.address).to eq('Example adres')
        expect(page).to have_content('Thank you for your order.')
      end
    end

    context 'fail' do
      it 'create new order' do
        visit '/'

        click_link 'Dodaj do koszyka'
        click_link 'Checkout'

        expect(page.current_path).to eq(new_order_path('pl'))

        within('form#new_order') do
          click_button 'Utwórz'
        end

        expect(page.current_path).to eq(orders_path('pl'))
        expect(page).to have_content('prohibited this article from being saved:')
      end

      it 'try to create order without product' do
        visit new_order_path
        expect(page.current_path).to eq(root_path('pl'))
        expect(page).to have_content('Your cart is empty!')
      end
    end
  end

end

require "rails_helper"

RSpec.feature "Listing Shelf" do
  before do
    @damian = User.create(email: "damian@example.com", password: "password")
    @product1 = Product.create(title: "Product 1", description: "Lorem ipsum dolor sit amet, consectetur.", price: 19.99, user: @damian)
    @product2 = Product.create(title: "Product 2", description: "Body of second product.", price: 19.09, user: @damian)
    @product3 = Product.create(title: "Product 3", description: "Body of third product.", price: 19.88, user: @damian, shelf: false)
  end

  scenario "listing all products with shelf true" do
    visit "/"

    expect(page).to have_content(@product1.title)
    expect(page).to have_content(@product1.description)
    expect(page).to have_content(@product1.price)
    expect(page).to have_content(@product2.title)
    expect(page).to have_content(@product2.description)
    expect(page).to have_content(@product2.price)
    expect(page).not_to have_content(@product3.title)
    expect(page).not_to have_content(@product3.description)
    expect(page).not_to have_content(@product3.price)
  end
end
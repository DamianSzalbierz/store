require "rails_helper"

RSpec.feature "Edit a product" do
  let(:admin) { User.create(email: "damian@example.com", password: "password", admin: true) }
  let!(:product) { Product.create(title: "Product 1", description: "Lorem ipsum dolor sit amet, consectetur.", price: 19.99, user: admin) }
  before { login_as(admin) }

  scenario "An admin updates a product" do
    visit "/"
    click_link "Produkty"
    click_link product.title
    click_link "Edit Product"
    fill_in "Tytuł", with: "Update Title"
    fill_in "Opis", with: "Update Description"
    fill_in "Cena", with: 10
    click_button "Aktualizuj"
    expect(page).to have_content("Product has been updated")
    expect(page.current_path).to eq(product_path(product))
  end

  scenario "An admin fails to updates a product" do
    visit "/"
    click_link "Produkty"
    click_link product.title
    click_link "Edit Product"
    fill_in "Tytuł", with: ""
    fill_in "Opis", with: "Update Description"
    fill_in "Cena", with: 10
    click_button "Aktualizuj"
    expect(page).to have_content("Product has not been updated")
    expect(page.current_path).to eq(product_path(product))
  end
end
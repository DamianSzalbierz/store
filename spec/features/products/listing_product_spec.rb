require "rails_helper"

RSpec.feature "Listing Products" do
  let(:user) { User.create(email: "damian@example.com", password: "password")}
  let!(:product1) { Product.create(title: "Product 1", description: "Lorem ipsum dolor sit amet, consectetur.", price: 19.99, user: user)}
  let!(:product2) { Product.create(title: "Product 2", description: "Body of second product.", price: 19.09, user: user)}

  scenario "With product created and user not signed in" do
    visit products_path

    expect(page).to have_content('Musisz się zalogować lub zarejestrować się przed kontynuowaniem.')
  end

  scenario "With product created and user signed in" do
    login_as(user)
    visit products_path

    expect(page).to have_content(product1.title)
    expect(page).to have_content(product1.description)
    expect(page).to have_content(product1.price)
    expect(page).to have_content(product2.title)
    expect(page).to have_content(product2.description)
    expect(page).to have_content(product2.price)
    expect(page).to have_link(product1.title)
    expect(page).to have_link(product2.title)
  end

  scenario "A user has no artiles" do
    Product.delete_all

    login_as(user)
    visit products_path

    expect(page).not_to have_content(product1.title)
    expect(page).not_to have_content(product1.description)
    expect(page).not_to have_content(product1.price)
    expect(page).not_to have_content(product2.title)
    expect(page).not_to have_content(product2.description)
    expect(page).not_to have_content(product2.price)
    expect(page).not_to have_link(product1.title)
    expect(page).not_to have_link(product2.title)

    within('h1') do
      expect(page).to have_content("No Products Created")
    end
  end

end
require "rails_helper"

RSpec.feature "Delete a Product" do
  let(:admin) { User.create(email: "damian@example.com", password: "password", admin: true) }
  let!(:product) { Product.create(title: "Product 1", description: "Lorem ipsum dolor sit amet, consectetur.", price: 19.99, user: admin) }
  before { login_as(admin) }

  scenario "An admin successfully delete a product" do
    visit "/"

    click_link "Produkty"
    click_link product.title
    click_link "Delete Product"

    expect(page).to have_content("Product has been deleted")
    expect(page.current_path).to eq(products_path)
    expect(page).not_to have_content(product.title)
  end
end
require "rails_helper"

RSpec.feature "Creating Products" do
    let(:admin) { User.create(email: "damian@example.com", password: "password", admin: true) }
    before { login_as(admin) }

  scenario "An admin add new product" do
    visit '/'
    click_link "Produkty"
    expect(page.current_path).to eq(products_path)
    click_link "Nowy produkt"
    expect(page.current_path).to eq(new_product_path)
    fill_in "Tytuł", with: "Produkt 1"
    fill_in "Opis", with: "Lorem ipsum dolor sit amet"
    fill_in "Cena", with: 19.99
    click_button "Utwórz"
    expect(Product.last.user).to eq(admin)
    expect(page.current_path).to eq(products_path)
    expect(page).to have_content("Product has been created by #{admin.email}")
    expect(page).to have_content("Utworzone przez: #{admin.email}")
  end

  scenario "An admin fails to create a new product" do
    visit '/'
    click_link "Produkty"
    expect(page.current_path).to eq(products_path)
    click_link "Nowy produkt"
    expect(page.current_path).to eq(new_product_path)
    click_button "Utwórz"
    expect(page).to have_content("Product has not been created")
    expect(page).to have_content("prohibited this article from being saved:")
  end
end
require "rails_helper"

RSpec.feature "Show Product" do
    let(:admin) { User.create(email: "damian@example.com", password: "password", admin: true) }
    let(:user) { User.create(email: "kuba@example.com", password: "password") }
    let!(:product) { Product.create(title: "Product 1", description: "Lorem ipsum dolor sit amet, consectetur.", price: 19.99, user: admin) }

  scenario "to non-signed in users hide edit and delete button" do
    expect(page).not_to have_link("Produkty")
    visit products_path
    expect(page).to have_content('Musisz się zalogować lub zarejestrować się przed kontynuowaniem.')
  end

  scenario "to signed in users hide edit and delete button" do
    login_as(user)
    expect(page).not_to have_link("Produkty")
    visit products_path
    click_link product.title
    expect(page.current_path).to eq(product_path(product))
    expect(page).to have_content(product.title)
    expect(page).to have_content(product.description)
    expect(page).to have_content(product.price)
    expect(page).not_to have_link("Edit Product")
    expect(page).not_to have_link("Delete Product")
  end

  scenario "to signed in admin show edit and delete button" do
    login_as(admin)
    visit "/"
    click_link "Produkty"
    click_link product.title
    expect(page.current_path).to eq(product_path(product))
    expect(page).to have_content(product.title)
    expect(page).to have_content(product.description)
    expect(page).to have_content(product.price)
    expect(page).to have_link("Edit Product")
    expect(page).to have_link("Delete Product")
  end
end
require "rails_helper"

RSpec.feature "Signing users out" do
  let(:damian) {User.create(email: "damian@example.com", password: "password")}
  before {login_as damian}

  scenario do
    visit '/'
    click_link "Wyloguj"

    expect(page).to have_content("Wylogowanie jest zrealizowane pomyślnie.")
    expect(page).not_to have_link("Wyloguj")
  end
end
require "rails_helper"

RSpec.feature "Signup user" do

  scenario "Valid user signup" do
    visit "/"
    click_link "Zarejestruj"
    fill_in "Email", with: "user@example.com"
    fill_in "Password", with: "password"
    fill_in "Password confirmation", with: "password"
    click_button "Sign up"

    expect(page).to have_content("Witamy! Zarejestrowałeś pomyślnie.")
  end

  scenario "Invalid user signup" do
    visit "/"
    click_link "Zarejestruj"
    click_button "Sign up"

    expect(page).to have_css('div.panel.panel-danger')
    expect(page).to have_content("Nie zapisane")
  end

end
require "rails_helper"

RSpec.feature "Users signin" do
  let!(:damian) {User.create(email: "damian@example.com",
                             password: "password",
                             password_confirmation: "password")}

  scenario "Sign in with valid credentials" do
    visit "/"

    click_link "Zaloguj"
    fill_in "Email", with: damian.email
    fill_in "Password", with: damian.password
    click_button "Log in"

    expect(page).to have_content("Logownie pomyślnie zrealizowane.")
    expect(page).to have_content("Aktualnie jako #{damian.email}")
    expect(page).not_to have_link("Sign in")
    expect(page).not_to have_link("Sign up")
    expect(page).to have_link("Wyloguj")
  end

  scenario "Sign in with invalid credentials" do
    visit "/"

    click_link "Zaloguj"
    fill_in "Email", with: "testtest@test@test.sd"
    fill_in "Password", with: damian.password
    click_button "Log in"

    expect(page).to have_content("Nieprawidłowe Email lub hasło.")
    expect(page).to have_button("Log in")
  end

end
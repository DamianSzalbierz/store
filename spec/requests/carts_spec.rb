require 'rails_helper'

RSpec.describe "Carts", type: :request do
  describe "GET /cart" do
    before do
      @cart1 = Cart.create
    end

    it "works! (now write some real specs)" do
      get cart_path('pl', @cart1.id)
      expect(response).to have_http_status(200)
    end

    it 'redirect when cart not found' do
      get cart_path('pl', 'xxx')
      expect(response).to have_http_status(302)
      flash_message = "The cart you are looking for could not be found"
      expect(flash[:alert]).to eq(flash_message)
    end
  end
end

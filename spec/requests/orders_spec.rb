require 'rails_helper'

RSpec.describe "Orders", type: :request do
  describe "GET /orders" do
    before do
      @damian = User.create(email: "damian@example.com", password: "password", admin: true)
      login_as @damian
    end

    it "works! (now write some real specs)" do
      get orders_path
      expect(response).to have_http_status(200)
    end
  end
end

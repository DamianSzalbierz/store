require 'rails_helper'

RSpec.describe "Users", type: :request do
  describe "GET /users" do
    before do
      @damian = User.create(email: "damian@example.com", password: "password", admin: true)
      login_as @damian
    end

    it "works! (now write some real specs)" do
      get users_admin_index_path
      expect(response).to have_http_status(200)
    end
  end
end

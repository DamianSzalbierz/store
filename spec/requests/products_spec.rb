require "rails_helper"

RSpec.describe "Products", type: :request do
  before do
    @damian = User.create(email: "damian@example.com", password: "password", admin: true)
    @kasia = User.create(email: "kasia@example.com", password: "password")
    @product = Product.create!(title: "Product 1", description: "Lorem ipsum dolor sit amet, consectetur.", price: 19.99, user: @damian)
  end

  describe "GET /products/:id" do
    context "with existing product" do
      before do
        login_as @damian
        get "/products/#{@product.id}"
      end

      it "handles existing product" do
        expect(response.status).to eq(200)
      end
    end

    context "with non existing product" do
      before do
        login_as @damian
        get "/products/xxx"
      end

      it "handles non existing product" do
        expect(response.status).to eq(302)
        flash_message = "The product you are looking for could not be found"
        expect(flash[:alert]).to eq(flash_message)
      end
    end
  end

  describe "GET /products/:id/edit" do
    context "with non-signed in user" do
      before { get "/products/#{@product.id}/edit" }

      it "redirects to the signin page" do
        expect(response.status).to eq 302
        flash_message = "Musisz się zalogować lub zarejestrować się przed kontynuowaniem."
        expect(flash[:alert]).to eq flash_message
      end
    end

    context "with signed-in users who is non-owner" do
      before do
        login_as @kasia
        get "/products/#{@product.id}/edit"
      end

      it "redirects to home page" do
        expect(response.status).to eq 302
        flash_message = "You can only edit your own product."
        expect(flash[:alert]).to eq flash_message
      end
    end

    context "with signed-in users who is owner" do
      before do
        login_as @damian
        get "/products/#{@product.id}/edit"
      end

      it "give access to editing product" do
        expect(response.status).to eq 200
      end
    end
  end

  describe "DELETE /products/:id" do
    context "with non-signed in user" do
      before { delete "/products/#{@product.id}" }

      it "redirects to the signin page" do
        expect(response.status).to eq 302
        flash_message = "Musisz się zalogować lub zarejestrować się przed kontynuowaniem."
        expect(flash[:alert]).to eq flash_message
      end
    end

    context "with signed-in users who is non-owner" do
      before do
        login_as @kasia
        delete "/products/#{@product.id}"
      end

      it "redirect to the signin page" do
        expect(response.status).to eq 302
        flash_message = "You can only delete your own products."
        expect(flash[:alert]).to eq flash_message
      end
    end

    context "with signed-in users who is owner" do
      before do
        login_as @damian
        delete "/products/#{@product.id}"
      end

      it "delete product" do
        expect(response.status).to eq 302
        flash_message = "Product has been deleted"
        expect(flash[:success]).to eq flash_message
      end
    end
  end
end
require 'rails_helper'

RSpec.describe Order, type: :model do
  describe 'association' do
    it {is_expected.to have_many(:line_items).dependent(:destroy)}
  end

  describe 'validations' do
    it {validate_presence_of(:name)}
    it {validate_presence_of(:address)}
    it {validate_presence_of(:email)}
    it {is_expected.to define_enum_for(:pay_type).with([:check, :credit_card, :purchase_order])}
  end

  describe 'methods' do
    before do
      @cart = Cart.create
      @user = User.create(email: 'user@example.com', password: 'foobar', admin: true)
      @product = Product.create(title: 'Produkt 1', description: 'Opis produktu 1', price: 23)
      @cart.line_items.build(product: @product)
      @cart.line_items.build(product: @product)
      @cart.line_items.build(product: @product)
      @order = Order.create(name: 'Damian', address: 'Gorzów', email: @user.email)
    end

    it 'expect to add cart line_items to order' do
      @order.add_line_items_from_cart(@cart)
      @cart.reload
      expect(@cart.line_items.length).to eq(0)
      expect(@order.line_items.length).to eq(3)
    end

    it 'expect pay_types to show Czek, Karta kredytowa i Zlecenie zapłaty' do
      pay_types = [["Czek", "check"],
                   ["Karta kredytowa", "credit_card"],
                   ["Zlecenie zapłaty", "purchase_order"]]
      expect(Order.pay_types_i18n).to eq(pay_types)
    end
  end
end

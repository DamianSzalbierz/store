require 'rails_helper'

RSpec.describe Product, type: :model do
  describe 'validations' do
    before do
      @user = User.create(email: "admin@example.com", name: "John Admin", password: "foobar", password_confirmation: "foobar", admin: true)
      @product = Product.create(title: 'Produkt 1', description: 'Opis produktu', price: 45.00, user: @user)
    end

    it 'product is valid without image' do
      expect(@product).to be_valid
    end

    it 'product is valid with image' do
      @product.image = File.open(Rails.root.join("spec", "fixtures", "adrpo.jpg"))
      @product.save
      expect(@product).to be_valid
    end

    it { should validate_presence_of(:title) }
    it { should validate_presence_of(:description) }
    it { should validate_presence_of(:price) }
  end

  describe 'assertions' do
    it { is_expected.to belong_to(:user) }
    it { is_expected.to have_many(:line_items) }
    it { is_expected.to have_many(:orders).through(:line_items) }
  end

  describe 'methods' do
    before do
      @user = User.create(email: "admin@example.com", name: "John Admin", password: "foobar", password_confirmation: "foobar", admin: true)
      @product1 = Product.create(title: 'Produkt 1', description: 'Opis produktu', price: 21.00, user: @user)
      @product2 = Product.create(title: 'Produkt 2', description: 'Opis produktu', price: 45.00, user: @user)
      @product2 = Product.create(title: 'Produkt 3', description: 'Opis produktu', price: 45.00, user: @user)

      LineItem.create(product_id: @product1.id)
    end

    context 'test search method' do
      it 'should search products with phrase' do
        @products = Product.search('Produkt 1')
        expect(@products.length).to eq(1)
      end

      it 'should show all products without phrase' do
        @products = Product.search('')
        expect(@products.length).to eq(3)
      end
    end

    context 'test line_item_do_not_exist? method' do
      it 'should delete product without :line_items' do
        @product2.destroy
        expect(Product.all.length).to eq(2)
      end

      it 'should not delete product with :line_items' do
        @product1.destroy
        expect(Product.all.length).to eq(3)
      end
    end
  end
end

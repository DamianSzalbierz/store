require 'rails_helper'

RSpec.describe User, type: :model do
  describe 'associations' do
    it {is_expected.to have_many(:products)}
  end

  describe 'methods' do
    context 'admin_exist?' do
      it 'test without admin' do
        expect(User.admin_exist?).to be false
      end

      it 'test with admin' do
        User.create(email: 'admin@example.com', password: 'foobar', admin: true)
        expect(User.admin_exist?).to be true
      end
    end
  end
end

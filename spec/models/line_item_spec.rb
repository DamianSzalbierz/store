require 'rails_helper'

RSpec.describe LineItem, type: :model do
  describe 'associations' do
    it { is_expected.to belong_to(:product) }
    it { is_expected.to belong_to(:cart) }
    it { is_expected.to belong_to(:order) }
  end

  describe 'validations' do
    it { is_expected.to validate_presence_of(:product_id) }
  end

  describe 'methods' do
    before do
      @product = Product.create(title: 'Produkt 1', description: 'Opis produktu', price: 45.00)
      @line_item = LineItem.create(product_id: @product.id, quantity: 4)
    end

    it 'expect to calculate price depend on quantity' do
      price = @line_item.price_calculation
      expect(price).to eq(180)
    end
  end
end

require 'rails_helper'

RSpec.describe Cart, type: :model do
  describe 'associations' do
    it { is_expected.to have_many(:line_items).dependent(:destroy) }
  end

  describe 'methods' do
    before do
      @cart = Cart.create
      @product1 = Product.create(title: 'Produkt 1', description: 'Opis produktu', price: 45.00)
      @product2 = Product.create(title: 'Produkt 1', description: 'Opis produktu', price: 30.00)
      @line_item = LineItem.create(product_id: @product1.id, quantity: 4, cart_id: @cart.id)
      @line_item = LineItem.create(product_id: @product2.id, quantity: 3, cart_id: @cart.id)
    end

    it 'expect to calculate price depend on quantity' do
      price = @cart.price_calculation
      expect(price).to eq(270)
    end

    it 'is expected to add quantity to line_item instead of new line_item' do
      @cart.add_product(@product1).save
      @cart.add_product(@product1).save
      @cart.reload
      expect(@cart.line_items.length).to eq(2)
      expect(@cart.line_items.find_by(product_id: @product1.id).quantity).to eq(6)
      expect(@cart.line_items.find_by(product_id: @product2.id).quantity).to eq(3)
    end

    it 'expect to create new line_item if product with the same id was not found in cart' do
      product3 = Product.create(title: 'Produkt 3', description: 'Opis produktu', price: 10.00)
      @cart.add_product(product3)
      expect(@cart.line_items.length).to eq(3)
    end
  end
end
